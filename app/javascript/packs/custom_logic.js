$(function () {
    $('input[name=search]').on('input', startSpinnerAndSearch);

    $('input[name=price]').on('input', startSpinnerAndSearch);

    $('select[name=price_range]').on('change', startSpinnerAndSearch);

    $('select[name=country]').on('change', startSpinnerAndSearch);

    $('select[name=sort_by]').on('change', startSpinnerAndSearch);

    function startSpinnerAndSearch() {
        startSpinner();
        search();
    }
});

function startSpinner() {
    $("#spinner-search").show();
}

function search() {
    $('#search-button').click();
}