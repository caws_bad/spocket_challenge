class HomeController < ApplicationController
  def index
    @product_tsvs = ProductTsv
                        .order('RANDOM()')
                        .includes(:product)
                        .page(0)
                        .per(8)
    @countries = ProductTsv.countries
    @sort_types = ProductTsv.sort_types
    @price_ranges = ProductTsv.price_ranges
  end
end
