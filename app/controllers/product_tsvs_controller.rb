class ProductTsvsController < ApplicationController
  before_action :set_product_tsv, only: [:show, :edit, :update, :destroy]

  # GET /product_tsvs
  # GET /product_tsvs.json
  def index
    sleep(1)

    @product_tsv_searcher = ProductTsvs::ProductTsvSearcher.new(params: params)

    @product_tsvs = @product_tsv_searcher
                        .process
                        .page(params[:page])
                        .per(8)
  end

  # GET /product_tsvs/1
  # GET /product_tsvs/1.json
  def show
  end

  # GET /product_tsvs/new
  def new
    @product = ProductTsv.new
  end

  # GET /product_tsvs/1/edit
  def edit
  end

  # POST /product_tsvs
  # POST /product_tsvs.json
  def create
    @product = ProductTsv.new(product_tsv_params)

    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, notice: 'ProductTsv was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /product_tsvs/1
  # PATCH/PUT /product_tsvs/1.json
  def update
    respond_to do |format|
      if @product.update(product_tsv_params)
        format.html { redirect_to @product, notice: 'ProductTsv was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /product_tsvs/1
  # DELETE /product_tsvs/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to product_tsvs_url, notice: 'ProductTsv was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_product_tsv
    @product_tsv = ProductTsv.find(params[:id])
  end
end
