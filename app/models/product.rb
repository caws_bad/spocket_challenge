class Product < ApplicationRecord
  include PgSearch::Model
  after_save :update_tsv
  has_one :product_tsv

  private

  def update_tsv
    if product_tsv.present?
      product_tsv.update(tsv: get_tsv)
    else
      ProductTsv.create(tsv: get_tsv, product: self)
    end
  end

  def get_tsv
    ActiveRecord::Base
        .connection
        .execute(
            "SELECT (to_tsvector('english', coalesce('#{title}', '')) ||
           to_tsvector('english', coalesce('#{description}', '')) ||
           to_tsvector('english', coalesce('#{country}', ''))) AS tsvector"
        )
        .first['tsvector']
  end
end
