class ProductTsv < ApplicationRecord
  include PgSearch::Model
  belongs_to :product

  # ID column is not taken into consideration when using the tsvector_column
  pg_search_scope :search,
                  against: :id,
                  using: {
                      tsearch: {
                          prefix: true,
                          dictionary: 'english',
                          tsvector_column: 'tsv'
                      }
                  }

  pg_search_scope :search_with_relevance,
                  associated_against: {
                      product: {
                          title: 'A',
                          description: 'B',
                          country: 'C',
                          tags: 'D'
                      }
                  },
                  using: {
                      tsearch: {
                          prefix: true
                      }
                  }

  scope :order_by_price, ->(order_type: 'ASC') {
    joins(:product)
        .order(price: order_type.to_sym)
  }

  scope :price_equal_to, ->(value) {
    joins(:product)
        .where('price = ?', value)
  }

  scope :price_less_than, ->(value) {
    joins(:product)
        .where('price < ?', value)
  }

  scope :price_higher_than, ->(value) {
    joins(:product)
        .where('price > ?', value)
  }

  scope :filter_by_country, ->(country_name) {
    joins(:product)
        .where('country = ?', country_name)
  }

  scope :countries, lambda {
    joins(:product)
        .pluck('products.country')
        .uniq << 'All'
  }

  def self.text_search(search_value, relevance: false)
    return all unless search_value.present?

    if relevance
      search_with_relevance(search_value)
    else
      search(search_value)
    end
  end

  def self.sort_types
    [
        %w[None none],
        %w[Relevance relevance],
        %w[Newest newest],
        %w[Lowest Price, lowest_price],
        %w[Highest Price, highest_price]
    ].freeze
  end

  def self.price_ranges
    [
        %w[None none],
        %w[Less than, less_than],
        %w[Equals to, equals_to],
        %w[Higher than, higher_than]
    ].freeze
  end

  private

  def associated_products_table_name(value)
    if value
      'products'
    else
      'products_product_tsvs'
    end
  end
end
