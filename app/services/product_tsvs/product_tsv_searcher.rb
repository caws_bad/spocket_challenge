module ProductTsvs
  class ProductTsvSearcher
    attr_accessor :params

    def initialize(params:)
      @params = params
      @search_results = ProductTsv.all
    end

    def process
      process_price_range
      process_origin_country
      process_sort_by

      process_search
    end

    private

    attr_accessor :search_results

    def process_price_range
      return if @params[:price_range].blank?
      return if @params[:price].blank?

      case @params[:price_range]
      when 'less_than'
        @search_results = @search_results
                              .price_less_than(@params[:price])
      when 'equals_to'
        @search_results = @search_results
                              .price_equal_to(@params[:price])
      when 'higher_than'
        @search_results = @search_results
                              .price_higher_than(@params[:price])
      end
    end

    def process_origin_country
      return if @params[:country].blank?
      return if @params[:country] == 'All'

      @search_results = @search_results
                            .filter_by_country(@params[:country])
    end

    def process_sort_by
      return if @params[:sort_by].blank?
      return if @params[:sort_by] == 'none'

      case @params[:sort_by]
      when 'lowest_price'
        @search_results = @search_results
                              .order_by_price
      when 'highest_price'
        @search_results = @search_results
                              .order_by_price(order_type: 'DESC')
      end
    end

    def process_search
      @search_results = @search_results
                            .includes(:product)
                            .text_search(
                                params[:search],
                                relevance: @params[:sort_by] == 'relevance'
      )
    end
  end
end