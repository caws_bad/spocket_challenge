class CreateProductTsvs < ActiveRecord::Migration[6.0]
  def change
    create_table :product_tsvs do |t|
      t.tsvector :tsv
      t.references :product, null: false, foreign_key: true

      t.timestamps
    end
  end
end
