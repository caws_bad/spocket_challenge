# Spocket - Challenge

### The search strategy 

I decided to leverage PostgreSQL's fulltext search via the pg_search gem.

However, instead of adding a column of tsvector type to the Product table itself, I decided
to go with an association that is basically used for searches in order to offload the Products table. 

### Getting started

```
Rails 6.0.0
Ruby 2.6.3
```

### Getting Started

* Clone the project
* rake:db create db:migrate db:seed
* bundle install
* rails s

### Built With
   * [pg_search](https://github.com/Casecommons/pg_search)
   * [Factory_bot](https://github.com/thoughtbot/factory_bot)
   * [Faker](https://github.com/stympy/faker)
   * [Postgres](https://bitbucket.org/ged/ruby-pg/wiki/Home)
   * [Rails](https://github.com/rails/rails)
   * [Rspec-rails](https://github.com/rspec/rspec)
   * [Shoulda-callback-matchers](https://github.com/jdliss/shoulda-callback-matchers)
   * [Shoulda-matchers](https://github.com/thoughtbot/shoulda-matchers/)
   