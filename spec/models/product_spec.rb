require 'rails_helper'

RSpec.describe Product, type: :model do
  let!(:product) { FactoryBot.create :product }
  let!(:product_params) { FactoryBot.attributes_for :product }
  it 'create an associated ProductTsv if none is present' do
    new_product = described_class.new(product_params)
    new_product.save
    expect(new_product.product_tsv).not_to be(nil)
  end

  it 'update an associated ProductTsv if present' do
    expect { product.update(title: 'Something') }.to change(product.product_tsv, :updated_at)
  end
end
