require 'rails_helper'

RSpec.describe ProductTsv, type: :model do
  before(:all) do
    0.upto(10) do
      FactoryBot.create(:product)
    end
  end
  let(:most_common_word_in_set) { common_word }
  let!(:product_not_listed) {
    FactoryBot.create(:product,
                      title: Faker::Lorem.word,
                      description: Faker::Lorem.sentence,
                      country: Faker::Lorem.word,
                      price: -50)
  }
  let!(:word_from_a_title) { Product.first.title.split.sample }

  describe 'scopes' do
    describe '#search' do
      it 'return products with a given word' do
        expect(
            described_class.search(word_from_a_title)
        ).to include { Product.first.product_tsv }
      end

      it 'fail to return products that do not contain a given word' do
        expect(
            described_class.search(word_from_a_title)
        ).not_to include { :product_not_listed }
      end
    end

    describe '#search_with_relevance' do
      it 'return a product that contain a given word in the title in the first position' do
        expect(
            described_class
                .search_with_relevance(word_from_a_title)
                .first
                .product
                .title
                .split
        ).to include { word_from_a_title }
      end

      it 'not return products that do not contain a given word' do
        expect(
            described_class.search_with_relevance(word_from_a_title)
        ).not_to include { :product_not_listed }
      end
    end

    describe '#order_by_price' do
      it 'return products ordered by price in ascending order' do
        ordered_products = described_class
                               .order_by_price
        expect(
            ordered_products
                .first
                .product
                .price
        ).to be < ordered_products
                      .second
                      .product.price
      end

      it 'return products ordered by price in ascending order where the first product'\
          'is not more expensive than the second one' do
        ordered_products = described_class
                               .order_by_price
        expect(
            ordered_products
                .first
                .product
                .price
        ).not_to be > ordered_products
                          .second
                          .product.price
      end

      it 'return products ordered by price in descending order' do
        ordered_products = described_class
                               .order_by_price(order_type: 'desc')
        expect(
            ordered_products
                .first
                .product
                .price
        ).to be > ordered_products
                      .second
                      .product.price
      end

      it 'return products ordered by price in descending order where the first product'\
          'is not less expensive than the second one' do
        ordered_products = described_class
                               .order_by_price(order_type: 'desc')
        expect(
            ordered_products
                .first
                .product
                .price
        ).not_to be < ordered_products
                          .second
                          .product.price
      end
    end

    describe '#price_equal_to' do
      it 'return products whose price is equal to a given value' do
        expect(
            described_class
                .price_equal_to(Product.first.price)
        ).to include { Product.first.product_tsv }
      end

      it 'not return products whose price is not equal to a given value' do
        expect(
            described_class
                .price_equal_to(Product.first.price)
        ).not_to include { :product_not_listed }
      end
    end

    describe '#price_less_than' do
      it 'return products whose price less than a given value' do
        product = Product.all.sample

        expect(
            described_class
                .price_less_than(product.price)
        ).not_to include { product.product_tsv }
      end

      it 'not return products whose price is not less than a given value' do
        product = Product.all.sample

        expect(
            described_class
                .price_less_than(product.price)
        ).not_to include { product.product_tsv }
      end
    end

    describe '#price_higher_than' do
      it 'return products whose price is higher than a given value' do
        product = Product.all.sample

        expect(
            described_class
                .price_higher_than(product.price)
                .first
                .product
                .price
        ).to be > product.price
      end
    end

    describe '#filter_by_country' do
      it 'return products for a given country' do
        product = Product.all.sample

        expect(
            described_class
                .filter_by_country(product.country)
        ).to include { product }
      end

      it 'not return a prodcut that does not belong to a given country' do
        product = Product.all.sample

        expect(
            described_class
                .filter_by_country(product.country)
        ).not_to include { :product_not_listed }
      end
    end

    describe '#countries' do
      it 'return a country list based on all products' do
        expect(
            described_class.countries
        ).to include { product_not_listed.country }
      end
    end

    describe '#text_search' do
      context 'normal search' do
        it 'return products with a given word' do
          expect(
              described_class.text_search(word_from_a_title)
          ).to include { Product.first.product_tsv }
        end


        it 'fail to return products that do not contain a given word' do
          expect(
              described_class.text_search(word_from_a_title)
          ).not_to include { :product_not_listed }
        end
      end

      context 'taking relevance into consideration' do
        it 'return a product that contain a given word in the title in the first position' do
          expect(
              described_class
                  .text_search(word_from_a_title, relevance: true)
                  .first
                  .product
                  .title
                  .split
          ).to include { word_from_a_title }
        end

        it 'not return products that do not contain a given word' do
          expect(
              described_class.text_search(word_from_a_title, relevance: true)
          ).not_to include { :product_not_listed }
        end
      end
    end
  end
end
