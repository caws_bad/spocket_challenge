FactoryBot.define do
  factory :product do
    title { Faker::Movies::LordOfTheRings.location.gsub('\'', '') }
    description { Faker::Movies::LordOfTheRings.quote.gsub('\'', '') }
    country { Faker::Address.country }
    tags { "MyString" }
    price { rand(0.5..6000.0).round(2) }
  end
end
