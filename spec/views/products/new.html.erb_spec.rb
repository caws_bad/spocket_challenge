require 'rails_helper'

RSpec.describe "products/new", type: :view do
  before(:each) do
    assign(:product, Product.new(
      :title => "MyString",
      :description => "MyString",
      :country => "MyString",
      :tags => "MyString",
      :price => 1.5
    ))
  end

  it "renders new product form" do
    render

    assert_select "form[action=?][method=?]", products_path, "post" do

      assert_select "input[name=?]", "product[title]"

      assert_select "input[name=?]", "product[description]"

      assert_select "input[name=?]", "product[country]"

      assert_select "input[name=?]", "product[tags]"

      assert_select "input[name=?]", "product[price]"
    end
  end
end
