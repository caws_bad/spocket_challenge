require 'rails_helper'

RSpec.describe "products/edit", type: :view do
  before(:each) do
    @product = assign(:product, Product.create!(
      :title => "MyString",
      :description => "MyString",
      :country => "MyString",
      :tags => "MyString",
      :price => 1.5
    ))
  end

  it "renders the edit product form" do
    render

    assert_select "form[action=?][method=?]", product_path(@product), "post" do

      assert_select "input[name=?]", "product[title]"

      assert_select "input[name=?]", "product[description]"

      assert_select "input[name=?]", "product[country]"

      assert_select "input[name=?]", "product[tags]"

      assert_select "input[name=?]", "product[price]"
    end
  end
end
